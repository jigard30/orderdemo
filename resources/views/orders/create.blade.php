@extends('layouts.app')

@section('content')
    <div class="container h-100 mt-5">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-10 col-md-8 col-lg-6">
                <h3>Add Order</h3>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                @endif
                <form action="{{ route('orders.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="user">User</label>
                        <select name="user_id" id="user_id" class="form-control">
                            <option value="">Select User</option>
                            @foreach ($users as $key => $user)
                                <option value="{{ $key }}">{{ $user }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div name="add_name" id="add_name">
                        <label for="product">Product</label>
                        <table class="table table-bordered table-hover" id="dynamic_field">
                          <tr>
                            <td><input type="text" name="product[0][name]" data-val="0" placeholder="Product Name" class="form-control product_name product_name-0" /></td>
                            <td><input type="text" name="product[0][quantity]" data-val="0" placeholder="Product Quantity" class="form-control product_quantity product_quantity-0"/></td>
                            <td><input type="text" name="product[0][amount]" data-val="0" value="" placeholder="Product Amount" class="form-control product_amount product_amount-0"/></td>
                            <td><input type="text" name="product[0][total]" data-val="0" value="" placeholder="Product Total" class="form-control product_total product_total-0" readonly/></td>
                            <td><button type="button" name="add" id="add" class="btn btn-primary add">+</button></td>
                          </tr>
                        </table>
                        {{-- <input type="submit" class="btn btn-success" name="submit" id="submit" value="Submit"> --}}
                    </div>
                    <div class="col-md-1"></div>
                    <div>
                        <p>Total: <span class="addtotal"></span></p>
                    </div>
                    {{-- <div class="form-group">
                        <label for="product">Product</label>
                        <select name="product_id" id="product_id" class="form-control" multiple>
                            <option value="">Select Product</option>
                            @foreach ($products as $key => $product)
                                <option value="{{ $key }}">{{ $product }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <br>
                    <button type="submit" class="btn btn-primary">Create Order</button>
                </form>
            </div>
        </div>
    </div>
    <script>
         $(document).ready(function(){

   var i = 1;
     var length;
     //var addamount = 0;
    var addamount = 700;

//     product_quantity
// product_amount
    let addtotal = 0;
    $(document).on('keyup','.product_quantity',function(){
        let i = $(this).data('val');
        console.log(i,Number($('.product_quantity-'+i).val()) , Number($('.product_amount-'+i).val()));
        if(i != undefined){
            $('.product_total-'+i).val(Number($('.product_quantity-'+i).val()) * Number($('.product_amount-'+i).val()));
        } else {
            $('.product_total').val(Number($(this).val() * $('.product_amount').val()));
        }
        getTotal();

    })
    $(document).on('keyup','.product_amount',function(){
        let i = $(this).data('val');
        console.log(i,Number($('.product_amount-'+i).val()) , Number($('.product_quantity-'+i).val()));

        if(i != undefined){
            $('.product_total-'+i).val($('.product_amount-'+i).val() * $('.product_quantity-'+i).val());
        } else {
            $('.product_total').val($(this).val() * $('.product_quantity').val());
        }
        getTotal();
    })

    function getTotal(){
        let total = $('.product_total').map((_,el) => el.value).get();
        const sum = total.reduce((partialSum, a) => Number(partialSum) + Number(a), 0);
        $('.addtotal').text(sum);
    }

   $(document).on('click',".add",function(){

    i++;
       $('#dynamic_field').append(`
        <tr id="row${i}" class="row${i}">
            <td><input type="text" name="product[${i}][name]" placeholder="Product Name" data-val="${i}" class="form-control product_name product_name-${i}" /></td>
            <td><input type="text" name="product[${i}][quantity]" placeholder="Product Quantity" data-val="${i}" class="form-control product_quantity product_quantity-${i}"/></td>
            <td><input type="text" name="product[${i}][amount]" value="" placeholder="Product Amount" data-val="${i}" class="form-control product_amount product_amount-${i}"/></td>
            <td><input type="text" name="product[${i}][total]" data-val="${i}" value="" placeholder="Product Total" class="form-control product_total product_total-${i}" readonly/></td>
            <td style="display: flex;justify-content: center;"><button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove" style="background:red;">X</button></td>
        </tr>`);
     });

   $(document).on('click', '.btn_remove', function(){
     addamount -= 700;
     console.log('amount: ' + addamount);

       var button_id = $(this).attr("id");
       $('#row'+button_id+'').remove();
     });



     $("#submit").on('click',function(event){
     var formdata = $("#add_name").serialize();
       console.log(formdata);

       event.preventDefault()

       $.ajax({
         url   :"action.php",
         type  :"POST",
         data  :formdata,
         cache :false,
         success:function(result){
           alert(result);
           $("#add_name")[0].reset();
         }
       });

     });
   });
    </script>
@endsection
