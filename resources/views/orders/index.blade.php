@extends('layouts.app')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container-fluid">
            <a class="navbar-brand h1" href={{ route('orders.index') }}>Orders</a>
            <div class="justify-end ">
                <div class="col ">
                    <a class="btn btn-sm btn-success" href={{ route('orders.create') }}>Add Order</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="row">
            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-success">
                        <th scope="col">Id</th>
                        <th scope="col">User name</th>
                        <th scope="col">Total</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <th scope="row">{{ $order->id }}</th>
                            <td>{{ $order->user->name }}</td>
                            <td>{{ array_sum($order->products->where('order_id',$order->id)->pluck('total')->toArray()) }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td><a href="{{ route('orders.show', [$order->id]) }}">View</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $orders->links() !!}
        </div>
    </div>
@endsection
