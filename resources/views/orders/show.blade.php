@extends('layouts.app')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container-fluid">
            <a class="navbar-brand h1" href={{ route('orders.index') }}>Show Orders</a>
            <div class="justify-end ">
                <div class="col ">
                    <a class="btn btn-sm btn-success" href={{ route('orders.index') }}>Back</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="row">
            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-success">
                        <th scope="col">Order Id</th>
                        <th scope="col">Order Grand Total</th>
                        <th scope="col">Order User</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-success">
                        <th scope="col">{{$order->id}}</th>
                        <th scope="col">{{array_sum($order->products->where('order_id',$order->id)->pluck('total')->toArray())}}</th>
                        <th scope="col">{{$order->user->name}}</th>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-success">
                        <th>Product Name</th>
                        <th>Product Quantity</th>
                        <th>Product Amount</th>
                        <th>Product Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->products->where('order_id',$order->id)->get() as $item)
                        <tr class="table-success">
                            <th scope="col">{{$item->name}}</th>
                            <th scope="col">{{$item->quantity}}</th>
                            <th scope="col">{{$item->amount}}</th>
                            <th scope="col">{{$item->total}}</th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
