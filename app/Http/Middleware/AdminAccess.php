<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next,$userType): Response
    {
        $role = auth()->user()->role == '0' ? 'admin' : 'user';
        if($role == $userType){
            return $next($request);
        }
        return redirect('403');
        // return response()->json(['You do not have permission to access for this page.']);
    }
}
