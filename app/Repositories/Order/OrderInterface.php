<?php

namespace App\Repositories\Order;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface OrderInterface
{

    public function getAllOrders();
    public function getUserProductData();
    public function storeOrder($request);
    public function storeProduct($product);
    public function showOrder($id);

}
