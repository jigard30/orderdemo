<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Order;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Order\OrderInterface;

class OrderRepository implements OrderInterface
{

    /**
     * @var \App\Models\Order
     */
    private $order;
    private $user;
    private $product;

    /**
     * OrderRepository constructor.
     *
     * @param \App\Models\Order $smallOrder
     */
    public function __construct(Order $order,User $user,Product $product)
    {
        $this->order = $order;
        $this->user = $user;
        $this->product = $product;
    }

    /**
     *
     * @return mixed
     */
    public function getAllOrders()
    {
        return $this->order::with('user', 'products')->paginate(10);
    }

     /**
     *
     * @return mixed
     */
    public function getUserProductData()
    {
        return $this->user::where('role', '1')->pluck('name', 'id')->toArray();
    }

    /**
     *
     * @return mixed
     */
    public function storeOrder($request)
    {
        return $this->order::create($request->except('_token','product'));
    }

    /**
     *
     * @return mixed
     */
    public function storeProduct($product)
    {
        return $this->product::create($product);
    }

    /**
     *
     * @return mixed
     */
    public function showOrder($id)
    {
        return $this->order::find($id);
    }

}
