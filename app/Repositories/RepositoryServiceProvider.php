<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Order\OrderInterface;
use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Support\DeferrableProvider;


class RepositoryServiceProvider extends ServiceProvider implements DeferrableProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerOrderRepository();
    }

    protected function registerOrderRepository()
    {
        $this->app->bind(OrderInterface::class, function () {
            return new OrderRepository(new Order(),new User(),new Product());
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            OrderInterface::class,
        ];
    }
}
