<?php

namespace App\Services\Order;


use App\Repositories\Order\OrderInterface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class OrderServices
{
    /**
     * @var \App\Services\Order\orderRepo
     */
    private $orderRepo;

    /**
     * OrderServices constructor.
     *
     * @param \App\Repositories\Order\OrderInterface $orderRepo
     */
    public function __construct(OrderInterface $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    /**
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getAllOrders()
    {
        return $this->orderRepo->getAllOrders();
    }

     /**
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUserProductData()
    {
        return $this->orderRepo->getUserProductData();
    }

    /**
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeData($request)
    {

        $order = $this->orderRepo->storeOrder($request);
        $products = $request->product;
        foreach($products as $product){
            $product['order_id'] = $order->id;
            $this->orderRepo->storeProduct($product);
        }
        return ['status' => 'success'];
    }

     /**
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function showOrder($id)
    {
        return $this->orderRepo->showOrder($id);
    }


}
