<?php

namespace App\Services\Order\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Order\OrderServices;
use App\Repositories\Order\OrderInterface;
use Illuminate\Contracts\Support\DeferrableProvider;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class OrderServicesProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();
    }

    protected function registerService()
    {

        $this->app->bind(OrderServices::class, function ($app) {
            return new OrderServices($app[OrderInterface::class]);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [OrderServices::class];
    }
}
