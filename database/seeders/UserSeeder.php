<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // create admin
        User::updateOrCreate(['email' => 'admin@gmail.com'],[
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'role' => '0',
            'password' => Hash::make('Admin@123')
        ]);

        // create users
        \App\Models\User::factory(5)->create();
    }
}
