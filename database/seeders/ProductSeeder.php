<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ([1,2,3,4,5] as $item) {
            Product::updateOrCreate(['name' => 'Product '.$item],[
                'order_id' => $item,
                'name' => 'Product '.$item,
                'quantity' => $item,
                'amount' => $item,
                'total' => $item * $item
            ]);
        }
    }
}
